import 'package:flutter/material.dart';

main() {
  runApp(MainPage());
}

class MainPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
          appBar: AppBar(
            title: Text("My App"),
          ),
          body:  Container(
              child: Column(
                  children: <Widget>[
                    Container(
                      height: 200,
                      child: Card(
                        color: Colors.blue,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10)
                        ),
                        child: Container(),
                      ),
                    ),
                    Container(
                      height: 200,
                      child: Card(
                        color: Colors.green[300],
                        child: Container(),
                      ),
                    ),
                    Container(
                      height: 200,
                      child: Card(
                        color: Colors.orange,
                        child: Container(),
                      ),
                    ),
                   

                  ],
                )
            ),
        )
        
    );
  }
}
