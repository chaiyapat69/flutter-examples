import 'package:flutter/material.dart';

main() {
  runApp(MainPage());
}

class MainPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: AppBar(
          title: Text("In My App"),
          bottom: TabBar(
            tabs: <Widget>[
              Tab(icon: Icon(Icons.directions_car), text: "Tab 1"),
              Tab(icon: Icon(Icons.directions_transit), text: "Tab 2"),
              Tab(icon: Icon(Icons.directions_bus), text: "Tab 3")
            ],
          ),
        ),
        body: TabBarView(
          children: <Widget>[
            Icon(
              Icons.directions_car,
              size: 150,
              color: Colors.pink,
            ),
            Icon(
              Icons.directions_transit,
              size: 150,
              color: Colors.pink,
            ),
            Icon(
              Icons.directions_bus,
              size: 150,
              color: Colors.pink,
            )
          ],
        ),
        bottomNavigationBar: BottomNavigationBar(
          currentIndex: 0, // this will be set when a new tab is tapped
          items: [
            BottomNavigationBarItem(
              icon: new Icon(Icons.home),
              title: new Text('Home'),
            ),
            BottomNavigationBarItem(
              icon: new Icon(Icons.mail),
              title: new Text('Messages'),
            ),
            BottomNavigationBarItem(
                icon: Icon(Icons.person), title: Text('Profile'))
          ],
        ),
      ),
    ));
  }
}

class CardList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Card(
        margin: EdgeInsets.all(10),
        elevation: 20,
        child: Container(
          padding: EdgeInsets.all(20),
          child: Column(
            children: <Widget>[
              Container(
                padding: EdgeInsets.only(bottom: 10),
                child: Image.asset("assets/images/Yoda.jpg"),
              ),
              Container(
                padding: EdgeInsets.only(bottom: 10),
                child: Text("Title"),
              ),
              Container(
                  padding: EdgeInsets.only(bottom: 10),
                  child: Text("- DESC - ")),
            ],
          ),
        ));
  }
}
