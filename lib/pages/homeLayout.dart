import 'package:flutter/material.dart';

main() {
  runApp(MainPage());
}

class MainPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: _MyMainPage(),
    );
  }
}

class _MyMainPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("My Flutter"),
      ),
      body: new Container(
        color: Colors.green,
        height: 300,
        child: new Row(
          children: <Widget>[
            Container(
              color: Colors.red,
              margin: EdgeInsets.all(10),
              height: 100,
              width: 100,
              padding: EdgeInsets.all(20),
              child: new Column(
                children: <Widget>[Text("Column 1")],
              ),
            ),
            Container(
              color: Colors.yellow,
              margin: EdgeInsets.all(10),
              height: 100,
              width: 100,
              padding: EdgeInsets.all(20),
              child: new Column(
                children: <Widget>[Text("Column 2")],
              ),
            ),
            Container(
              color: Colors.blue,
              margin: EdgeInsets.only(left: 10),
              height: 100,
              width: 100,
              padding: EdgeInsets.all(20),
              child: new Column(
                children: <Widget>[Text("Column 3")],
              ),
            )
          ],
        ),
      ),
    );
  }
}

/*
import 'package:flutter/material.dart';

void main() {
  runApp(MainPage());
}

class MainPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("My App"),
        ),
        body: Container(
          color: Colors.green,
          height: 250,
          child: Row(
            children: <Widget>[
              Container(
                color: Colors.red[400],
                width: 100,
                height: 100,
                margin: EdgeInsets.only(right: 20, left: 20),
                child: Column(
                  children: <Widget>[
                    Text("Column 1")
                  ],
                ),
              ),
              Container(
                color: Colors.yellow[400],
                width: 100,
                height: 100,
                margin: EdgeInsets.only(left: 20),
                child: Column(
                  children: <Widget>[
                    Text("Column 2")
                  ],
                ),
              ),

              Container(
                color: Colors.blue[400],
                width: 100,
                height: 100,
                margin: EdgeInsets.only(left: 20),
                child: Column(
                  children: <Widget>[
                    Text("Column 2")
                  ],
                ),
              ),

             
            ],
          ),
        ) ,
      ),
    );
  }
}


*/
