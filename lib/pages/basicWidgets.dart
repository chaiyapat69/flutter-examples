import 'package:flutter/material.dart';

main() {
  runApp(MainPage());
}

 class MainPage extends StatelessWidget {
   @override
   Widget build(BuildContext context) {
     return MaterialApp(
       home: Scaffold(
         appBar: AppBar(
           title: Text("Myapp"),
         ),
         body: Column(
           children: <Widget>[
             Text("Text 1", 
                  style: TextStyle(
                      fontSize: 100, 
                      fontWeight: FontWeight.bold, 
                      color: Colors.green)
                  ),
             Image.asset("assets/images/Yoda.jpg"),
             Image.network("https://s.isanook.com/hi/0/rp/r/w728/ya0xa0m1w0/aHR0cHM6Ly9zLmlzYW5vb2suY29tL2hpLzAvdWQvMjk3LzE0ODg1NzcveW91dHViZS1wcmVtaXVtLmpwZw==.jpg"),
             Icon(Icons.directions_bus, size: 100, color: Colors.pink,)
           ],
         )
         ),       
     );
   }
 }