import 'package:flutter/material.dart';

main() {
  runApp(MainPage());
}

class MainPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "My Flutter",
      home: _Mainpage(),
    );
  }
}

//ตัวเก็บข้อมูล
class Quote {
  final String title;
  final String desc;
  Quote(this.title, this.desc);
}

class _Mainpage extends StatefulWidget {
  @override
  __MainpageState createState() => __MainpageState();
}

class __MainpageState extends State<_Mainpage> {
  List<Quote> quotes = [];
  final _formkey = GlobalKey<FormState>();

  String _inputTitle;
  String _inputDesc;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("My Flutter"),
          backgroundColor: Colors.greenAccent,
        ),
        body: Column(
          children: <Widget>[
            Form(
              key: _formkey,
              child: Column(
                children: <Widget>[
                  TextFormField(
                    decoration: InputDecoration(labelText: "Title"),
                    onSaved: (String value) {
                      _inputTitle = value;
                    },
                  ),
                  TextFormField(
                      decoration: InputDecoration(labelText: "Desc"),
                      onSaved: (String value) {
                        _inputDesc = value;
                      }),
                  RaisedButton(
                    elevation: 5,
                    onPressed: () {
                      _formkey.currentState.save(); //เพื่อ save ข้อมูล
                      // debugPrint(_inputTitle);
                      // debugPrint(_inputDesc);
                      setState(() {
                        //setState เพื่อ refresh widget ทั้งหมด
                        quotes.insert(
                            0,
                            Quote(_inputTitle,
                                _inputDesc)); //Data ใหม่จะต่อด้านบน
                      });

                      // quotes.add(Quote(_inputTitle, _inputDesc)); // dataใหม่จะต่อท้าย
                      _formkey.currentState
                          .reset(); //เพื่อ reset ข้อมุลที่อยู่ใน box
                    },
                    child: Text("SAVE"),
                  )
                ],
              ),
            ),
            Expanded(
              child: quotes.length == 0
                  ? Center(
                      child: Text("No Data"),
                    )
                  : ListView.builder(
                      itemCount: quotes.length,
                      itemBuilder: (BuildContext contex, int index) {
                        return CardList(
                            quotes[index].title, quotes[index].desc);
                      },
                    ),
            )
          ],
        ));
  }
}

class CardList extends StatelessWidget {
  final String _title;
  final String _desc;

  const CardList(
    this._title,
    this._desc, {
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
        margin: EdgeInsets.all(10),
        elevation: 20,
        child: Container(
          padding: EdgeInsets.all(20),
          child: Column(
            children: <Widget>[
              Container(
                padding: EdgeInsets.only(bottom: 10),
                child: Image.asset("assets/images/Yoda.jpg"),
              ),
              Container(
                padding: EdgeInsets.only(bottom: 10),
                child: Text("$_title"),
              ),
              Container(
                  alignment: Alignment.bottomRight,
                  padding: EdgeInsets.only(bottom: 10),
                  child: Text("- $_desc - ")),
            ],
          ),
        ));
  }
}
