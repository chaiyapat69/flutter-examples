import 'dart:io';

import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:http/http.dart'
    as http; //Install => https://pub.dev/packages/http
// import 'package:cached_network_image/cached_network_image.dart'; // https://pub.dev/packages/cached_network_image#-installing-tab-
// import 'package:flutter_cache_store/flutter_cache_store.dart'; //https://pub.dev/packages/flutter_cache_store#-installing-tab-

main() {
  runApp(MainPage());
}

class newsLists {
  final String title;
  final String img;
  newsLists({this.title, this.img});
  factory newsLists.fromJson(Map<String, dynamic> parsedJson) {
    return newsLists(title: parsedJson["title"], img: parsedJson["urlToImage"]);
  }
}

class MainPage extends StatefulWidget {
  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  List<newsLists> _newsLists = [];
  var isLoading = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    loadData();
  }

  loadData() async {
    setState(() {
      isLoading = true;
    });
    // final response = await http.get(
    //     "https://apimb.sanook.com/recommend/c3a700119873bdf57820dc5935b14295/8.0?startwith=create_date&additionalFields",
    //     headers: {"Accept": "application/json"});
    final response = await http.get(
        "https://newsapi.org/v2/top-headlines?country=th&apiKey=35c3b4efae044206a41f0d24724170e5",
        headers: {"Accept": "application/json"});

    if (response.statusCode == 200) {
      final responseJSON = json.decode(response.body);
      setState(() {
        _newsLists = (responseJSON['articles'] as List)
            .map((data) => new newsLists.fromJson(data))
            .toList();
        isLoading = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
      appBar: AppBar(
        title: Text("My Flutter"),
        backgroundColor: Colors.greenAccent,
      ),
      body: isLoading
          ? Center(
              child: CircularProgressIndicator(
                valueColor:
                    new AlwaysStoppedAnimation<Color>(Colors.greenAccent),
              ),
            )
          : ListView.builder(
              itemCount: _newsLists.length,
              itemBuilder: (BuildContext contex, int index) {
                return CardLists(
                    _newsLists[index].title, _newsLists[index].img, "");
              },
            ),
    ));
  }
}

class CardLists extends StatelessWidget {
  final String _title;
  final String _date;
  final String _img;

  const CardLists(
    this._title,
    this._img,
    this._date, {
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 8,
      margin: EdgeInsets.all(8),
      child: Container(
        height: 100,
        child: Row(
          children: <Widget>[
            Container(
              padding: EdgeInsets.all(5),
              width: 150,
              child: _img == null
                  ? Text(
                      "No image",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: Colors.grey,
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                    )
                  : Image.network("$_img"),
            ),
            Container(
              width: 200,
              margin: EdgeInsets.all(10),
              child: Column(
                children: <Widget>[
                  Text(
                    "$_title",
                    textAlign: TextAlign.left,
                    style: TextStyle(fontSize: 12, fontWeight: FontWeight.bold),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
