import 'package:flutter/material.dart';

main() {
  runApp(MainPage());
}

 class MainPage extends StatelessWidget {
   @override
   Widget build(BuildContext context) {
     return MaterialApp(
theme: ThemeData.dark(),
       home: Scaffold(
         appBar: AppBar(
           title: Text("Myapp"),
         ),
         body: Column(
           children: <Widget>[
             Card(
               margin: EdgeInsets.all(10),
               elevation: 20,
               child: Container( 
                 padding: EdgeInsets.all(20),
                 child: Column(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(bottom:10),
                      child:Image.asset("assets/images/Yoda.jpg"),
                    ),
                    Container(
                      padding: EdgeInsets.only(bottom:10),
                      child: Text("What is Lorem Ipsum?" ),
                    ),
                    Container(
                       padding: EdgeInsets.only(bottom:10),
                      child:Text("Lorem Ipsum is simply dummy text of the printing and typesetting industry.")
                    ),
                  ],
               ),
               )
             )
           ],
         )
         ),       
     );
   }
 }

 /// V2
 /// VDO => https://www.youtube.com/watch?v=xdpnyU1hoWI&list=PLklsd8s-TIh6QeaZA39oDQfwq4rKQCqiJ&index=4
 /* import 'package:flutter/material.dart';
 คลิกรูปหลอดไฟเหลือง => Extract widget
import 'package:flutter/material.dart';
main() {
  runApp(MainPage());
}

class MainPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "My Flutter",
      home: _Mainpage(),
    );
  }
}

class _Mainpage extends StatefulWidget {
  @override
  __MainpageState createState() => __MainpageState();
}

class __MainpageState extends State<_Mainpage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("My Flutter"),
      ),
      body: ListView(
           children: <Widget>[
             CardList("title1", "Desc1"),
             CardList("title2", "Desc2"),
             CardList("title3", "Desc3"),
             CardList("title4", "Desc4"),
             CardList("title5", "Desc5"),
             CardList("title6", "Desc6"),
           ],
         ),
      
    );
  }
}

class CardList extends StatelessWidget {
  final String _title;
  final String _desc;

  const CardList(this._title, this._desc,{
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.all(10),
      elevation: 20,
      child: Container( 
        padding: EdgeInsets.all(20),
        child: Column(
         children: <Widget>[
           Container(
             padding: EdgeInsets.only(bottom:10),
             child:Image.asset("assets/images/Yoda.jpg"),
           ),
           Container(
             padding: EdgeInsets.only(bottom:10),
             child: Text("$_title"),
           ),
           Container(
              padding: EdgeInsets.only(bottom:10),
             child:Text("$_desc")
           ),
         ],
      ),
      )
    );
  }
}


*/