import 'package:flutter/material.dart';

main() {
  runApp(MainPage());
}

class MainPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("My App"),
        ),
        body: Container(
          width: double.infinity,
          color: Colors.green[200],
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                "Page 1",
                textAlign: TextAlign.center,
              )
            ],
          ),
        ),
      ),
    );
  }
}
